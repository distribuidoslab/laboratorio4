# Instrucciones

* Instalar WSL en Windows (https://www.youtube.com/watch?v=5RTSlby-l9w)
* Luego de la instalación, abrir "Windows PowerShell" (ejecutar como administrador)
* En el PowerShell, escribir el comando "wsl"
* Ingresar al directorio, y ejecutar los test. La primera vez pide instalar GO (comando "sudo apt install golang-go")

También se puede ejecutar desde la aplicación de Ubuntu, o desde la terminal de Visual Studio Code.

